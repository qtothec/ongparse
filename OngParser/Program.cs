﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;

namespace OngParser
{
    class Program
    {
        static string getHTML(string patentNum)
        {
            // create the web request
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(
                @"http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL" +
                @"&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=1&f=G&l=50&s1=" +
                patentNum + ".PN.&OS=PN/" + patentNum + "&RS=PN/" + patentNum
                );
            request.KeepAlive = false;
            request.ServicePoint.Expect100Continue = false;
            System.Diagnostics.Debug.Write(request.RequestUri);

            // get the web response
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string html = String.Empty;
            using (StreamReader sr = new StreamReader(data))
            {
                html = sr.ReadToEnd();
            }
            return html;
        }

        static void Main(string[] args)
        {
            // Read in patent numbers from file
            TextWriter consoleOut = new StreamWriter(Console.OpenStandardOutput());
            consoleOut.WriteLine("Please enter location of IDs file: ");
            consoleOut.Flush();
            TextReader consoleIn = new StreamReader(Console.OpenStandardInput());
            String idFile = consoleIn.ReadLine();
            idFile = (String.IsNullOrEmpty(idFile))
                ? @"C:\Users\QtotheC\Downloads\IDs.txt" : idFile;
            TextReader input = new StreamReader(idFile);

            // set up Excel worksheet
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbooks workbooks = excelApp.Workbooks;
            Excel.Workbook workbook;
            consoleOut.WriteLine("Please enter location to save Excel file:");
            consoleOut.Flush();
            String saveLocation = consoleIn.ReadLine();
            saveLocation = (String.IsNullOrEmpty(saveLocation))
                ? @"C:\Users\QtotheC\Downloads\OngParse.xlsx" : saveLocation;
            if (File.Exists(saveLocation))
            {
                workbook = workbooks.Open(saveLocation);
            }
            else
            {
                workbook = workbooks.Add();
                workbook.SaveAs(saveLocation);
            }
            Excel.Worksheet worksheet = (Excel.Worksheet)workbook.Sheets[1];
            worksheet.Cells.Clear();
            worksheet.Cells[1, 1] = "Patent ID";
            worksheet.Cells[1, 2] = "Inventors";
            worksheet.Cells[1, 3] = "Assignees";
            worksheet.Cells[1, 4] = "Appl. No.";
            worksheet.Cells[1, 5] = "Filed:";
            int curRow = 2;

            // no more user input needed. Close input and consoleOut streams
            consoleOut.Close();
            consoleIn.Close();

            // set up regular expressions
            RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            Regex rInventors = new Regex(@"(?<=<tr>\s*<th[^>]*>Inventors:</th>\s*<td [^>]+>).*?(?=</td>\s*</tr>)", options);
            Regex rAssignee = new Regex(@"(?<=<tr>\s*<th[^>]*>Assignee:</th>\s*<td [^>]+>).*?(?=</td>\s*</tr>)", options);
            Regex rApplNo = new Regex(@"(?<=<tr>\s*<th[^>]*>Appl. No.:\s*</th>\s*<td [^>]+>).*?(?=</td>\s*</tr>)", options);
            Regex rFiled = new Regex(@"(?<=<tr>\s*<th[^>]*>Filed:\s*</th>\s*<td [^>]+>).*?(?=</td>\s*</tr>)", options);

            // start processing input
            string patentNum;
            while ((patentNum = input.ReadLine()) != null)
            {
                string html = getHTML(patentNum);
                // parse HTML content
                Match mInventors = rInventors.Match(html);
                String rawInventorValue = mInventors.Value.Trim();

                Match mAssignee = rAssignee.Match(html);
                String rawAssigneeValue = mAssignee.Value.Trim();

                Match mApplNo = rApplNo.Match(html);
                String rawApplNoValue = mApplNo.Value.Trim();

                Match mFiled = rFiled.Match(html);
                String rawFiledValue = mFiled.Value.Trim();

                // postprocess to remove extra tags and new lines
                rawInventorValue = rawInventorValue.
                    Replace("<B>", "").Replace("</B>", "").
                    Replace(System.Environment.NewLine, "");
                rawAssigneeValue = rawAssigneeValue.
                    Replace("<B>", "").Replace("</B>", "").
                    Replace(System.Environment.NewLine, "").
                    Replace("<BR>", "");
                rawApplNoValue = rawApplNoValue.
                    Replace("<B>", "").Replace("</B>", "").
                    Replace(System.Environment.NewLine, "");
                rawFiledValue = rawFiledValue.
                    Replace("<B>", "").Replace("</B>", "").
                    Replace(System.Environment.NewLine, "");

                worksheet.Cells[curRow, 1] = patentNum;
                worksheet.Cells[curRow, 2] = rawInventorValue;
                worksheet.Cells[curRow, 3] = rawAssigneeValue;
                worksheet.Cells[curRow, 4] = rawApplNoValue;
                worksheet.Cells[curRow, 5] = rawFiledValue;
                curRow++;
            }

            // close the IDs input file
            input.Close();

            // modify Excel formatting
            worksheet.UsedRange.WrapText = false;
            worksheet.Columns.AutoFit();

            // save and exit Excel
            workbook.Save();
            workbook.Close();
            workbooks.Close();
        }
    }
}
